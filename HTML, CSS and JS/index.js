const submit1 = () => {
    let inputString = document.forms[0].inputString.value;
    let includedString = document.forms[0].includedString.value;
    let splitString = document.forms[0].splitString.value;

    let isIncluded = inputString.includes(includedString)?"include":"doesn't include";
    let subString = inputString.split(splitString);
    let outputString = `Length of input is ${inputString.length} and "${inputString}" ${isIncluded} "${includedString}" and the input after slplitting is: "${subString}"`;
    document.getElementById("resultString").innerHTML = outputString;
}

const submit2 = () =>{
    let objectText = document.forms[1].objectText.value;
    let obj={};
    let KeyVal = objectText.split(",");

    for (i in KeyVal) {
        KeyVal[i] = KeyVal[i].split(":");
        obj[KeyVal[i][0]]=KeyVal[i][1];
    }

    let inputConverted = JSON.stringify(obj);
    document.getElementById("resultJSON").innerHTML = inputConverted;
}

const submit3 = () => {
    let elements = document.forms[2].arrayText.value.split(',');
    let elementsAddMore = document.forms[2].addArray.value.split(',');

    elementsAddMore.forEach( (ele) => elements.push(ele) );
    
    elements.forEach((element,index) => {
        document.getElementById("resultArray").innerHTML += index + ":" + element + "<br>";
    });
}

const today = () => {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); 
    let yyyy = today.getFullYear();

    today = `Today is: ${mm}/${dd}/${yyyy}`
    document.getElementById("today").innerHTML=today;
}

const map = () => {
    let elements = document.forms[3].inputNumber.value.split(',');
    console.log(elements);
    let squareEles = elements.map(ele => ele*ele);
    let sumSquare = squareEles.reduce((total,num) => total + num);
    let oddNums = elements.filter(ele => ele%2 != 0);

    document.getElementById("sumSquares").innerHTML=`Sum of Square is: ${sumSquare} <br> Odd numbers is: ${oddNums}`;
}

