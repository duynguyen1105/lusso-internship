import fb from "./facebook_icon-circle.svg";
import insta from "./instagram_icon-circle.svg";
import linkedin from "./linkedin_icon-circle.svg";
import twitter from "./twitter_icon-circle.svg";
import youtube from "./youtube_icon-circle.svg";


const logoSocial = [
    twitter,
    linkedin,
    insta,
    fb,
    youtube,
]
export default logoSocial;
