import dropbox from "./dropbox.svg";
import gmail from "./gmail.svg";
import jira from "./jira.svg";
import outlook from "./outlook.svg";
import slack from "./slack.svg";
import zoom from "./zoom.svg";
import dde from "./69dde9.png"
import fad from "./fad88f.png"


const inteLogo = [
    dropbox,
    gmail,
    jira,
    outlook,
    slack,
    zoom,
    dde,
    fad,
]
export default inteLogo;
