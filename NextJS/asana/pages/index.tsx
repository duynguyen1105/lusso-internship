import React from 'react'
import Layout from '../components/Layout/Layout'
import Marketing from '../components/Marketing/Marketing'

const Home = () => {
  return (
    <Layout>
      <Marketing/>
    </Layout>
  )
}

export default Home
