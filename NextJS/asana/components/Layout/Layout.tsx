import React from 'react'
import Head from 'next/head'
import Header from '../Header/Header'
import styles from './Layout.module.scss'
import Footer from '../Footer/Footer'

const Layout = ({children}) => {
    return (
        <div>
            <Head>
                <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
                <meta charSet="UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Asana</title>
            </Head>
            <header>
                <Header/>
            </header>
            <main>
                <div className={styles.content}>
                    {children}
                </div>
            </main>            
            <footer>
                <Footer/>
            </footer>
        </div>
    )
}

export default Layout
