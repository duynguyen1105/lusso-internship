import React from 'react'
import styles from './Marketing.module.scss'
import Link from 'next/link'
import Image from 'next/image'
import pic1 from '../../assets/marketing/pic1.png'
import logoSection from '../../assets/marketing/logoSection2/index'
import Introduce from './Introduce/Introduce'
import {introMarketing} from '../../lib/introduceMarketing'
import inteLogo from '../../assets/marketing/intergration/index'
import {templateItems} from '../../lib/templateItem'

const Marketing = () => {
    return (
        <div className={styles.marketing}>

            {/* section 1*/}
            <section className={styles.sectionBuilder1}>
                <div className={styles.container}>
                    <div className={styles.rowAlignItem}>
                        <div className={styles.leftCol}>
                            <div className={styles.textStack}>
                                <div className={styles.label}>
                                    For Marketing and Creative Teams
                                </div>
                                <div className={styles.heading}>
                                    Your secret to standout marketing
                                </div>
                                <p>
                                    Simplify processes, reduce busywork, and lay out clear goals and plans with Asana work management software—so you and your team can focus on the work you do best.
                                </p>
                                <div className={styles.button1}>
                                    <Link href="/">
                                        <a>
                                            Try for free
                                        </a>
                                    </Link>
                                </div>
                                <div className={styles.button2}>
                                    <Link href="/">
                                        <a>
                                            Request a demo
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className={styles.rightCol}>
                            <img src={pic1} className={styles.pic1} />
                        </div>
                    </div>
                </div>
            </section>

            {/* section 2 */}
            <section className={styles.sectionBuilder2}>
                <div className={styles.container}>
                    <div className={styles.row}>
                        <div className={styles.col}>
                            <div className={styles.heading}>
                                <p>
                                    See why more than a million teams in 190 countries trust Asana.
                                </p>
                            </div>
                        </div>
                        <div className={styles.col} >
                            <div className={styles.colLogo}>
                                {
                                    logoSection.map( logo => (
                                        <div className={styles.customerLogo} key={logoSection.indexOf(logo)}>
                                            <div className={styles.logo} >
                                                <img src={logo} alt="Logo customer"/>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {/* section 3 */}
            <section className={styles.sectionBuilder3}>
                {
                    introMarketing.map(ele => (
                        <Introduce ele={ele} key={ele.id}/>
                    ))
                }
            </section>

            {/* section 4 */}
            <section className={styles.sectionBuilder4}>
                <div className={styles.container}>
                    <div className={styles.row}>
                        <div className={styles.banner}>
                            <p>
                                Do more of the marketing work you love. Let us help you get started.
                            </p>
                            <Link href="/">
                                <a>
                                    Request a demo
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>

            {/* section 5 */}
            <section className={styles.sectionBuilder5}>
                <div className={styles.container}>
                    <div className={styles.row}>
                        <div className={styles.col}>
                            <div className={styles.textStack}>
                                <div className={styles.label}>
                                    Process management
                                </div>
                                <div className={styles.heading}>
                                    Solutions for your most important workflows
                                </div>
                                <p className={styles.subheading}>
                                    From kickoff to sign-off, reduce the time and effort it takes to deliver better campaigns, product launches, events, and more.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {/* section 6 */}
            <section className={styles.sectionBuilder6}>
                <div className={styles.container}>
                    <div>
                        <div className={styles.col}>
                            <div className={styles.textStack}>
                                <div className={styles.label}>
                                    Integrations
                                </div>
                                <div className={styles.heading}>
                                    Keep all your data and dots connected
                                </div>
                                <p className={styles.subheading}>
                                Integrate the apps you rely on every day with Asana, and keep all your tools, info, and processes in one place.
                                </p>
                                <div className={styles.logoInline}>
                                    {
                                        inteLogo.map(e=>(
                                            <Link href="/" key={inteLogo.indexOf(e)}>
                                                <a className={styles.logoImg}>
                                                    <img src={e} alt="Logo"/>
                                                </a>
                                            </Link>
                                        ))
                                    }
                                </div>
                                <Link href="/">
                                    <a className={styles.seeMore}>See more integrations</a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            {/* section 7 */}
            <section className={styles.sectionBuilder7}>
                <div className={styles.container}>
                    <div className={styles.row}>
                        <div className={styles.col}>
                            <header className={styles.templateHeader}>
                                <div className={styles.label}>
                                    Templates
                                </div>
                                <div className={styles.title}>
                                    Don’t start from scratch
                                </div>
                                <p className={styles.description}>
                                    Check out the range of easy-to-use templates we have for popular projects and processes. Then, customize them for your workflows.
                                </p>
                            </header>
                            <div className={styles.templateRow}>
                                <div className={styles.templateItems}>
                                    {templateItems.map(item => (
                                        <Link href="/" key={templateItems.indexOf(item)}>
                                            <a className={styles.item}>
                                                <img src={item.image} alt="Image template"/>
                                                <p className={styles.itemInfo}>
                                                    <span className={styles.itemName}>{item.name}</span>
                                                    <span className={styles.itemType}>{item.type}</span>
                                                </p>
                                            </a>
                                        </Link>
                                    ))}
                                </div>
                                <Link href="/">
                                    <a className={styles.templateAll}>
                                        <div className={styles.videoContainer}>
                                            <video src="https://d1gwm4cf8hecp4.cloudfront.net/videos/product-automation/templates/view-all-templates.mp4" muted autoPlay loop className={styles.video}></video>
                                        </div>
                                        <span className={styles.text}>
                                            View all templates
                                        </span>
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>  
                <div className={styles.divider}>
                    <hr/>
                </div>
            </section>

            {/* section 8 */}
            <section className={styles.sectionBuilder8}>
                <div className={styles.container}>
                    <div className={styles.row}>
                        <div className={styles.col}>
                            <div className={styles.textStack}>
                                <div className={styles.heading}>
                                Resources that work for you
                                </div>
                                <p className={styles.subheading}>
                                Learn more about Asana, and discover why we’re a leader in collaborative work management.
                                </p>
                            </div>
                        </div>
                        <div className={styles.content}>
                            <div className={styles.container}>
                                <div className={styles.resources}>
                                    <div className={styles.singleCard}>
                                        <div className={styles.imageCard}>
                                            <div className={styles.imageCardText}>
                                                <div className={styles.label}>
                                                    The Anatomy of Work
                                                </div>
                                                <p>Find out how people spend their time at work, and what’s working and what isn’t.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div className={styles.singleCard}>
                                        <div className={styles.imageCard}>
                                            <div className={styles.imageCardText}>
                                                <div className={styles.label}>
                                                The Marketer’s Playbook
                                                </div>
                                                <p>Plan and execute your marketing strategies effortlessly with this guide.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div className={styles.singleCard}>
                                        <div className={styles.imageCard}>
                                            <div className={styles.imageCardText}>
                                                <div className={styles.label}>
                                                Watch the Webinar
                                                </div>
                                                <p>Learn how Drift runs integrated marketing campaigns in a remote world.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div className={styles.singleCard}>
                                        <div className={styles.imageCard}>
                                            <div className={styles.imageCardText}>
                                                <div className={styles.label}>
                                                The Ultimate Guide
                                                </div>
                                                <p>Managing an end-to-end marketing campaign requires meticulous attention to detail. Download the guide.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {/* section 9 */}
            <section className={styles.sectionBuilder9}>
                <div className={styles.container}>
                    <div className={styles.col}>
                        <div className={styles.textStack}>
                            <div className={styles.heading}>
                            Get started today
                            </div>
                            <p className={styles.subHeading}>
                            Spend more time with your team pursuing big ideas and creating positive buzz for your brand.
                            </p>
                            <button className={styles.button}>
                                Talk to sales
                            </button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Marketing
