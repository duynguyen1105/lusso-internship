import React from 'react'
import styles from './Introduce.module.scss'
import Link from 'next/link'

const Introduce = ({ele}) => {
    return (
        <div className={`${styles.container} ${ele.id%2!=0 && styles.reverse}`}>
            <div className={styles.rowAlignItems}>
                <div className={styles.leftCol}>
                    <img src={ele.image} alt=""/>
                </div>
                <div className={styles.rightCol}>
                    <div className={styles.textStack}>
                        <div className={styles.label}>
                            {ele.label}
                        </div>
                        <div className={styles.heading}>
                            {ele.heading}
                        </div>
                        <p>
                            {ele.subheading}
                        </p>
                        <div className={styles.quote}>
                            {ele.quote[0]}<Link href="/"><a>{ele.quote[1]}</a></Link>{ele.quote[2]}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Introduce
