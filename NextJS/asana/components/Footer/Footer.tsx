import React from 'react'
import styles from './Footer.module.scss'
import logo from '../../assets/footer/logo.png'
import {footerList} from '../../lib/footerList'
import logoLanguage from '../../assets/header/language.svg'
import Image from 'next/image'
import logoSocial from '../../assets/footer/social/index'
import appStore from '../../assets/footer/app-store.svg'
import ggplay from '../../assets/footer/ggplay.svg'
import Link from 'next/link'

const Footer = () => {
    return (
        <div className={styles.siteFooter}>
            <div className={styles.mainWrapper}>
                <div className={styles.container}>
                    <div className={styles.row}>
                        <div className={styles.logo}>
                            <Link href="/">
                                <a>
                                    <img src={logo} alt="Logo Asana"/>
                                </a>
                            </Link>
                        </div>
                        {
                            footerList.map(list => (
                                <ul className={styles.footerList} key={footerList.indexOf(list)}>
                                    <li className={styles.listTitle}>
                                        {list.title}
                                    </li>
                                    {list.item.map(item=>(
                                        <li className={styles.listItem} key={list.item.indexOf(item)}>
                                            <Link href="/">
                                                <a>
                                                    {item}
                                                </a>
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            ))
                        }
                    </div>
                </div>        
            </div>
            <div className={styles.secondWrapper}>
                <div className={styles.container}>
                    <div className={styles.row}>
                        <div className={styles.colWrapper}>
                            <div className={styles.language}>
                                <div className={styles.globe}>
                                    <Image
                                        src={logoLanguage}
                                        className={styles.logoLanguage}
                                        alt="Logo Language Selector"
                                        width={25}
                                        height={25}        
                                        color='white'                        
                                    />
                                </div>
                                <div className={styles.modalLink}>
                                    <Link href="/">
                                        <a className={styles.hoveredText}>
                                            English
                                        </a>
                                    </Link>
                                </div>
                            </div>
                            <div className={styles.privacy}>
                                <Link href="/">
                                    <a  className={styles.hoveredText}>
                                        Terms & Privacy
                                    </a>
                                </Link>
                            </div>
                            <div className={styles.social}>
                                {logoSocial.map(e=>(
                                    <Link href="/" key={logoSocial.indexOf(e)}> 
                                        <a >
                                            <img src={e} alt=""/>
                                        </a>
                                    </Link>
                                ))}
                            </div>
                            <div className={styles.downloadButton}>
                                <Link href="/">
                                    <a>
                                        <img src={appStore} alt="Download by App Store"/>
                                    </a>
                                </Link>
                                <Link href='/'>
                                    <a>
                                        <img src={ggplay} alt="Download by Google Play"/>
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    )
}

export default Footer
