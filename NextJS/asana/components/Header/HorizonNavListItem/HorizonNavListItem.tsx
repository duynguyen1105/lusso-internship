import React from 'react'
import styles from './HorizonNavListItem.module.scss'
import Image from 'next/image'
import caret from '../../../assets/header/horizonNavListItem/caret.svg'
import NavDropDown from './NavDropDown/NavDropDown'

const HorizonNavListItem = ({item}) => {
    return (
        <div className={styles.listItem}>
            <div className={styles.navToggle}>
                <div className={styles.navText}>
                    {item.label}
                </div>
                {item.dropDown && (
                    <div className={styles.caret}>
                        <Image
                            src={caret}
                            width={10}
                            height={10}
                        />
                    </div>
                )}
            </div>
        
            {item.dropDown && <NavDropDown/> }       
            
        </div>
    )
}

export default HorizonNavListItem
