import React from 'react'
import styles from './NavDropDown.module.scss'
import Link from 'next/link'

const NavDropDown = () => {
    return (
        <div className={styles.navDropDown}>
            <div className={styles.sectionList}>
                <div className={styles.section}>
                    <h3 className={styles.sectionLabel}>
                        Overview
                    </h3>
                    <div className={styles.columnWrapper}>
                        <ul className={styles.column}>
                            <li className={styles.item}>
                                <div className={styles.dropDownCard}>
                                    <div className={styles.cardDescription}>
                                        <Link href="/">
                                            <a>
                                                <div className={styles.cardLable}>
                                                    Asana Overview
                                                </div>
                                                <p className={styles.cardSummary}>
                                                Build project plans, coordinate tasks, and hit deadlines
                                                </p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>    
                            </li>

                            <li className={styles.item}>
                                <div className={styles.dropDownCard}>
                                    <div className={styles.cardDescription}>
                                        <Link href="/">
                                            <a>
                                                <div className={styles.cardLable}>
                                                    Asana Overview
                                                </div>
                                                <p className={styles.cardSummary}>
                                                Build project plans, coordinate tasks, and hit deadlines
                                                </p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>    
                            </li>

                            <li className={styles.item}>
                                <div className={styles.dropDownCard}>
                                    <div className={styles.cardDescription}>
                                        <Link href="/">
                                            <a>
                                                <div className={styles.cardLable}>
                                                    Asana Overview
                                                </div>
                                                <p className={styles.cardSummary}>
                                                Build project plans, coordinate tasks, and hit deadlines
                                                </p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>    
                            </li>
                        </ul>
                    </div>
                </div>
                



                <div className={styles.section}>
                    <h3 className={styles.sectionLabel}>
                        Overview
                    </h3>
                    <div className={styles.columnWrapper}>
                        <ul className={styles.column}>
                            <li className={styles.item}>
                                <div className={styles.dropDownCard}>
                                    <div className={styles.cardDescription}>
                                        <Link href="/">
                                            <a>
                                                <div className={styles.cardLable}>
                                                    Asana Overview
                                                </div>
                                                <p className={styles.cardSummary}>
                                                Build project plans, coordinate tasks, and hit deadlines
                                                </p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>    
                            </li>

                            <li className={styles.item}>
                                <div className={styles.dropDownCard}>
                                    <div className={styles.cardDescription}>
                                        <Link href="/">
                                            <a>
                                                <div className={styles.cardLable}>
                                                    Asana Overview
                                                </div>
                                                <p className={styles.cardSummary}>
                                                Build project plans, coordinate tasks, and hit deadlines
                                                </p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>    
                            </li>

                            <li className={styles.item}>
                                <div className={styles.dropDownCard}>
                                    <div className={styles.cardDescription}>
                                        <Link href="/">
                                            <a>
                                                <div className={styles.cardLable}>
                                                    Asana Overview
                                                </div>
                                                <p className={styles.cardSummary}>
                                                Build project plans, coordinate tasks, and hit deadlines
                                                </p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>    
                            </li>   

                            <li className={styles.item}>
                                <div className={styles.dropDownCard}>
                                    <div className={styles.cardDescription}>
                                        <Link href="/">
                                            <a>
                                                <div className={styles.cardLable}>
                                                    Asana Overview
                                                </div>
                                                <p className={styles.cardSummary}>
                                                Build project plans, coordinate tasks, and hit deadlines
                                                </p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>    
                            </li>
                        </ul>

                        <ul className={styles.column}>
                            <li className={styles.item}>
                                <div className={styles.dropDownCard}>
                                    <div className={styles.cardDescription}>
                                        <Link href="/">
                                            <a>
                                                <div className={styles.cardLable}>
                                                    Asana Overview
                                                </div>
                                                <p className={styles.cardSummary}>
                                                Build project plans, coordinate tasks, and hit deadlines
                                                </p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>    
                            </li>

                            <li className={styles.item}>
                                <div className={styles.dropDownCard}>
                                    <div className={styles.cardDescription}>
                                        <Link href="/">
                                            <a>
                                                <div className={styles.cardLable}>
                                                    Asana Overview
                                                </div>
                                                <p className={styles.cardSummary}>
                                                Build project plans, coordinate tasks, and hit deadlines
                                                </p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>    
                            </li>

                            <li className={styles.item}>
                                <div className={styles.dropDownCard}>
                                    <div className={styles.cardDescription}>
                                        <Link href="/">
                                            <a>
                                                <div className={styles.cardLable}>
                                                    Asana Overview
                                                </div>
                                                <p className={styles.cardSummary}>
                                                Build project plans, coordinate tasks, and hit deadlines
                                                </p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>    
                            </li>   

                            <li className={styles.item}>
                                <div className={styles.dropDownCard}>
                                    <div className={styles.cardDescription}>
                                        <Link href="/">
                                            <a>
                                                <div className={styles.cardLable}>
                                                    Asana Overview
                                                </div>
                                                <p className={styles.cardSummary}>
                                                Build project plans, coordinate tasks, and hit deadlines
                                                </p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>    
                            </li>
                        </ul>
                    </div>
                </div>



            </div>
            <div className={styles.featuredSectionList}>

            </div>
        </div>
    )
}

export default NavDropDown
