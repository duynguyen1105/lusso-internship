import React, { useEffect, useState } from 'react'
import styles from './Header.module.scss'
import Link from 'next/link'
import Image from 'next/image'
import HorizonNavListItem from './HorizonNavListItem/HorizonNavListItem'
import {navListItem} from '../../lib/navListItem'
import logoLanguage from '../../assets/header/language.svg'

const Header = () => {

    const [scroll, setScroll] = useState(false);
    //handle scroll => change navbar
    useEffect(() => {
    window.addEventListener("scroll", () => {
        setScroll(window.scrollY > 10);
    });
    }, []);

    return (
        <div className={styles.siteHeader}>
            <div className={`${styles.headerRow} ${scroll&& styles.isScrolling}`}>
                <div className={styles.headerWrapper}>
                    <div className={styles.navGroup}>
                        <Link href="/" >
                                <Image
                                    className={styles.logo}
                                    src="/logo.svg"
                                    alt="Logo of website"
                                    width={111}
                                    height={22}
                                />
                        </Link>
                        <div className={styles.horizonNav}>
                            <ul className={styles.horizonNavList}>
                                {
                                    navListItem.map(item => (
                                        <li key={item.id}>
                                            <HorizonNavListItem item={item}  />
                                        </li>
                                    ))
                                }
                            </ul>
                        </div>
                    </div>

                    <div className={styles.navGroup}>
                        <div className={styles.languageWrapper}>
                            <Link href="/">
                                <a>
                                    <Image
                                        src={logoLanguage}
                                        alt="Logo Language Selector"
                                        width={20}
                                        height={20}                                
                                    />
                                </a>
                            </Link>
                        </div>
                        <div className={styles.selectSeperator}/>
                        <Link href="/">
                            <a>
                                Contact Sales
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                Log in
                            </a>
                        </Link>
                        <div className={styles.button}>
                            <Link href="/">
                                <a>
                                    Try for free
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    )
}

export default Header
