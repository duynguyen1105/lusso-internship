import imageTemplate from '../assets/marketing/template/index'

export const templateItems = [
    {
        image: imageTemplate.image1,
        name: 'Web production & planning',
        type: 'Board View',
    },
    {
        image: imageTemplate.image2,
        name: 'Web design',
        type: 'Board View',
    },
    {
        image: imageTemplate.image3,
        name: 'Creative requests',
        type: 'List View',
    },
    {
        image: imageTemplate.image4,
        name: 'Editorial calendar',
        type: 'List View',
    },
    {
        image: imageTemplate.image5,
        name: 'Event planning',
        type: 'List View',
    },
    {
        image: imageTemplate.image6,
        name: 'Agency collaboration',
        type: 'List View',
    },
]