export const navListItem = [
    {
        id: 1,
        label: 'Why Asana?',
        dropDown: true,
    },
    {
        id: 2,
        label: 'Solutions',
        dropDown: true,
    },
    {
        id: 3,
        label: 'Resources',
        dropDown: true,
    },
    {
        id: 4,
        label: 'Enterprise',
        dropDown: false,
    },
    {
        id: 5,
        label: 'Pricing',
        dropDown: false,
    },
]