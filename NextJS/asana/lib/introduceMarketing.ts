import introImg from '../assets/marketing/introduce/index'

export const introMarketing = [
    {
        id: 0,
        image: introImg.image1,
        label: 'Set your marketing strategy',
        heading: 'Create a clear path of action',
        subheading: 'With Asana, you can make sure everyone knows your marketing goals and expectations. Then, your team is free to run wild with ideas, not deadlines.',
        quote: [
            'See how ',
            'G2 executes',
            ' their marketing strategy 2x faster',
        ]
    },
    {
        id: 1,
        image: introImg.image2,
        label: 'Manage your marketing programs',
        heading: 'Turn strategy into workable plans',
        subheading: 'Empower your team to seamlessly and efficiently plan, organize, and execute marketing activities from start to finish. All without missing a step.',
        quote: [
            'See how ',
            'InVision launches',
            ' 6x more campaigns every quarter',
        ]
    },
    {
        id: 2,
        image: introImg.image3,
        label: 'Streamline creative production',
        heading: 'Give your team more time to create',
        subheading: 'From routing requests to tracking approvals, manage your creative production processes and eliminate ad hoc tasks that waste time.',
        quote: [
            'Find out how ',
            'Stance reduced',
            ' creative production time by half',
        ]
    },
]