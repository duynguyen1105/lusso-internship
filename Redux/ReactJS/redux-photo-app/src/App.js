import logo from './logo.svg';
import './App.css';
import { Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from './components/Header';

function App() {
  return (
    <div className="photo-app">
      <Suspense fallback={<div>Loading...</div>}>
        <BrowserRouter>
          <Header/>

          
        </BrowserRouter>
      </Suspense>
    </div>
  );
}

export default App;
