import React from "react";
import { NavLink } from "react-router-dom";
import { Col, Container, Row } from "reactstrap";
import "./Header.scss";

const Header = () => {
    return (
        <header className="header">
            <Container>
                <Row className="justify-content-between">
                    <Col xs="auto">
                        <a
                            href="https://redux.js.org/"
                            className="headerLink headerTitle"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Redux App
                        </a>
                    </Col>

                    <Col xs="auto">
                        <NavLink
                            className="headerLink"
                            to="/photos"
                            activeClassName="headerLinkActive"
                        >
                            Redux Project
                        </NavLink>
                    </Col>
                </Row>
            </Container>
        </header>
    );
};

export default Header;
