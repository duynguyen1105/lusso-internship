import React from "react";
import { Route, Switch, useRouteMatch } from "react-router";
import NotFound from "../../../components/NotFound";
import AddEditPage from "./AddEdit/AddEditPage";
import MainPage from "./Main";

const Photo = () => {
    const match = useRouteMatch();
    return (
        <Switch>
            <Route exact path={match.url} component={MainPage} />

            <Route path={`${match.url}/add`} component={AddEditPage} />
            <Route path={`${match.url}/:photoId`} component={AddEditPage} />

            <Route component={NotFound} />
        </Switch>
    );
};

export default Photo;
