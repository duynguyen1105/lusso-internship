import keeley from "./about-keeleyImg@2x.webp";
import hunter from "./about-team-hunter-pic@2x.webp";
import jorden from "./about-team-hunter-pic@2x.webp";
import justin from "./about-team-justin-pic@2x.webp";
import maitrik from './about-team-maitrik-pic@2x.webp'
import ruben from './about-team-ruben-pic@2x.webp'
import tejas from './about-team-tejas-pic@2x.webp'
import hardik from './hardik-pic@2x.webp'
import dhaduk from './hiren-dhaduk-pic@2x.webp'
import jignesh from './jignesh-pic@2x.webp'
import mary from './maryImg@2x.webp'
import prayaag from './prayaag-pic@2x.webp'


const photoMember = {
    keeley,
    hunter,
    jorden,
    justin,
    maitrik,
    ruben,
    tejas,
    hardik,
    dhaduk,
    jignesh,
    mary,
    prayaag,
}
export default photoMember;