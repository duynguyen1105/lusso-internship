import icon1 from "./icon1.svg";
import icon2 from "./icon2.svg";
import icon3 from "./icon3.svg";
import icon4 from "./icon4.svg";
import img1 from './img1.webp';
import img2 from './img2.webp';
import img3 from './img3.webp';
import img4 from './img4.webp';
import img5 from './img5.webp';
import img6 from './img6.webp';


const imagePotential = {
    icon1,
    icon2,
    icon3,
    icon4,
    img1 ,
    img2 ,
    img3 ,
    img4 ,
    img5 ,
    img6 ,
}
export default imagePotential;