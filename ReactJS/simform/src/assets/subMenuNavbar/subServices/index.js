import ai from "./AI.svg";
import api from "./api.svg";
import app from "./app.svg";
import customsw from "./customsw.svg";
import ecom from './ecom.svg'
import entapp from './entapp.svg'
import mobileapp from './mobileapp.svg'
import swpro from './swpro.svg'
import swtest from './swtest.svg'
import web from './web.svg'


const imageServices = {
    ai,
    api,
    app,
    customsw,
    ecom,
    entapp,
    mobileapp,
    swpro,
    swtest,
    web
}
export default imageServices;