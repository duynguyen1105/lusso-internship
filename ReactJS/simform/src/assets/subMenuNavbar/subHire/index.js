import angular from "./angularDev.svg";
import aspNet from "./aspNet.svg";
import dedicated from "./dedicated.svg";
import dedicatedSw from "./dedicatedSw.svg";
import flutter from './flutter.svg'
import mobileApp from './mobileApp.svg'
import nodejs from './nodejs.svg'
import php from './php.svg'
import python from './python.svg'
import rails from './rails.svg'
import reactjs from './reactjs.svg'
import reactnative from './reactnative.svg'


const imageHire = {
    angular,
    aspNet,
    dedicated,
    dedicatedSw,
    flutter,
    mobileApp,
    nodejs,
    php ,
    python,
    rails,
    reactjs,
    reactnative,
}
export default imageHire;