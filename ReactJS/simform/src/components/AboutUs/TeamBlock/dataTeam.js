import photoMember from '../../../assets/teamMember/index'

export const dataTeam = [
    [
        {
            name: 'Prayaag Kasundra',
            position: '— COO',
            photo: photoMember.prayaag,
        },
        {
            name: 'Hiren Dhaduk',
            position: '— VP Of Technology',
            photo: photoMember.dhaduk,
        },
        {
            name: 'Hardik Shah',
            position: '— Senior Tech Consultant',
            photo: photoMember.hardik,
        },
    ],
    [
        {
            name: 'Jignesh Solanki',
            position: '— Director of Sales',
            photo: photoMember.jignesh,
        },
        {
            name: 'Maitrik Kataria',
            position: '— VP of Marketing',
            photo: photoMember.maitrik,
        },
        {
            name: 'Tejas Kaneriya',
            position: '— Senior Tech Consultant',
            photo: photoMember.tejas,
        },
    ],
    [
        {
            name: 'Justin Mitchell',
            position: '— PMO USA',
            photo: photoMember.justin,
        },
        {
            name: 'Ruben Ramirez',
            position: '— Tech Consultant',
            photo: photoMember.ruben,
        },
        {
            name: 'Jordan Walker',
            position: '— Tech Consultant',
            photo: photoMember.jorden,
        },
    ],
    [
        {
            name: 'Hunter Mckinley',
            position: '— Marketing Manager',
            photo: photoMember.hunter,
        },
        {
            name: 'Mary Hoag',
            position: '— Project Manager',
            photo: photoMember.mary,
        },
        {
            name: 'Keeley Byrnes',
            position: '— Business Development',
            photo: photoMember.keeley,
        },
    ],
];