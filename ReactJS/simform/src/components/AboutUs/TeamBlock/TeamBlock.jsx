import React from 'react'
import style from './style.module.scss'

const TeamBlock = ({data,index}) => {
    return (
        <div className={`${style.wrapper} ${index%2===1 && style.purpleWrapper}`} >
            <div className={style.teamListing}>
                <div className={style.listingBg}></div>
                <div className={style.singleMemberWrapper}>                
                    {
                        data.map(ele=>(
                            <div className={style.singleMember}>
                                <div className={style.heading}>
                                    <div className={style.name}>
                                        {ele.name}
                                    </div>
                                    <div className={style.position}>
                                        {ele.position}
                                    </div>
                                </div>
                                <div className={style.imgWrapper}>
                                    <img src={ele.photo} alt=""/>
                                </div>
                            </div>
                        ))
                    }

                </div>
            </div>
        </div>
    )
}

export default TeamBlock
