import React from 'react'
import style from './style.module.scss'
import {dataBlock} from'./dataBlock'

const StackBlock = () => {
    return (
        <>
            {dataBlock.map((ele, index)=>
                (
                    <div className={`${style.stackBlock} ${style[`blockNumber${index}`]}`}>
                        <div className={style.title}>
                            <>
                                {ele.title.map(x=>(<>{x} <br/></>))}
                                    
                            </>
                        </div>
                        <div className={style.list}>
                            {ele.data.map(x=>
                                (
                                    <div className={style.singleStack} >{x}</div>
                                )
                            )}
                        </div>
                    </div>
                ))}
        </>
        
    )
}

export default StackBlock
