export const dataBlock = [
    {
        title: ['Accelerate' ,'Digital Innovation'],
        data: ['Digital Transformation','Custom Software','Business Applications','Software Products','API/System Integration','Mobile Apps','SaaS','Consumer Apps',],
    },
    {
        title: ['Extend ' ,'Tech Capacity'],
        data: ['Proven Processes ','Remote Team Culture','QA & Testing','Mentors and Tech Leads','Code Quality','Effortless Hiring at Scale','Guaranteed Results','Security','Tools and Cloud Servicing',],
    },
    {
        title: ['Your ' ,'Business Goals'],
        data: ['Your Tech Team ','Digital Transformation Plans','Product Development','System Integration','Modernization'],
    },
    {
        title: ['Your ' ,'Organization'],
        data: ['Sales & Marketing ','Operations','Human Resource','Finance','Production','Research and Development',],
    },
];