import React from 'react'
import style from './style.module.scss'

const SingleRow = (dataSingleRow) => {
    const data = dataSingleRow.dataSingleRow
    return (
        <div className={style.singleRow}>
            <div className={style.content}>
                <div className={style.contentRow}>
                    <div className={style.icon}>
                        <img src={data.content.icon[0]} alt=""/>
                    </div>
                    <h4>
                        {data.content.h4[0]}
                    </h4>            
                    <p>
                        {data.content.p[0][0]} <strong>{data.content.p[0][1]}</strong> {data.content.p[0][2]} 
                    </p>
                </div>

                <div className={style.contentRow}>
                    <div className={style.icon}>
                        <img src={data.content.icon[1]} alt=""/>
                    </div>
                    <h4>
                        {data.content.h4[1]}
                    </h4>            
                    <p>
                        {data.content.p[1][0]} <strong>{data.content.p[0][1]}</strong> {data.content.p[1][2]} 
                    </p>
                </div>
            </div>
            <div className={style.img}>
                <div className={style.right}>
                    <div className={style.left}>
                        <div className={style.imgWrapper}>
                            <img src={data.img[0]} alt=""/>
                        </div>
                    </div>
                    <div className={style.row}>
                        <div className={style.imgWrapper}>
                            <img src={data.img[1]} alt=""/>
                        </div>
                    </div>
                    <div className={style.row}>
                        <div className={style.imgWrapper}>
                            <img src={data.img[2]} alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SingleRow
