import imagePotential from '../../../assets/potential/index'

export const dataSingleRow = [
    {
        content: {
            icon: [imagePotential.icon1,imagePotential.icon2,],
            h4:['Health Benefits','Mentorship and Learning'],
            p: [
                ['We work best when the people we care are healthy. Our',' health program',' empowers you to be worry-free.'],
                ['Never stop learning by getting an online or',' in-person mentoring',' on ANY topic you want to grow in.']
            ]
        },
        img : [imagePotential.img1,imagePotential.img2,imagePotential.img3,]
    },
    {
        content: {
            icon: [imagePotential.icon3,imagePotential.icon4,],
            h4:['Flexibility to work','Level up career'],
            p: [
                ['Stay',' inspired to work',' however and whatever technologies you find your best productive self in.'],
                ['Work with internationally recognized clients and take ownership of how you',' work to build things.']
            ]
        },
        img : [imagePotential.img4,imagePotential.img5,imagePotential.img6,]
    }
]