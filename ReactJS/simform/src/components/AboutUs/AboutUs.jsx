import React from 'react'
import style from './style.module.scss'
import banner from '../../assets/aboutus.png'
import StackBlock from './StackBlock/StackBlock'
import TeamBlock from './TeamBlock/TeamBlock'
import {dataTeam} from './TeamBlock/dataTeam'
import growthPath from '../../assets/own-growth-path@2x.webp'
import SingleRow from './SingleRow/SingleRow'
import {dataSingleRow} from './SingleRow/dataSingleRow'

const AboutUs = () => {
    return (
        //TODO: research background
        <div className={style.aboutUs}>
            <section className={style.section1}>
                <div className={style.container}>
                    <div className={style.bannerImg}>
                        <img src={banner} alt=""/>
                    </div>
                    <div className={style.bannerContent}>
                        <h1 className={style.title}>
                            The <span className={style.headingRed}> extended team</span> that is <br/>part of your team
                        </h1>
                        <p className={style.text}>
                            Nearly every organization will need to become a tech company in order to compete tomorrow. Yes, even yours. At Simform, we are on a mission to help companies develop competitiveness and agility using the software.<br /><br />We’re a tech company with a mission to help successful companies <strong>extend their tech capacity</strong>. Founded in 2010, we have helped organizations ranging from Startups that went public, to Fortune 500 companies, and WHO featured NGOs.<br /><br />Simform helps companies become innovation leaders by<strong>delivering software teams</strong> on demand. Our teams help you decide the right architecture and processes to follow and oversee the successful delivery of your software projects.
                        </p>
                        
                    </div>
                </div>  
            </section>

            <section className={style.section2}>
                <div className={style.container}>
                    <div className={style.headingWrapper}>
                        <h2>We are your <span className={style.highlightHead}>development stack</span></h2>
                    </div>
                    <div className={style.headingPara}>
                        <p>
                            You run your business. We take care of your development needs. Get access to an entire team of experts, ready <span className={style.colorThrough}>whenever you need us.</span>
                        </p>
                    </div>
                    <div className={style.stackListBlock}>
                        <StackBlock/>
                    </div>
                </div>
            </section>

            <section className={style.section3}>
                <div className={style.container}>
                    <div className={style.headingWrapper}>
                        <h2>Leadership <span className={style.highlightHead}>Team</span></h2>
                    </div>
                    <div className={style.teamListBlock}>
                        {
                            dataTeam.map((ele,index)=>
                                
                                <TeamBlock data={ele} index={index}/>
                            )
                        }

                    </div>
                </div>
            </section>

            <section className={style.section4}>
                <div className={style.container}>
                    <div className={style.content}>
                        <h3>Choose your own growth path</h3>
                        <a className={style.button} href=""><span>Explore our openings</span></a>
                    </div>
                    <div className={style.img}>
                        <img src={growthPath} alt=""/>
                    </div>
                </div>
            </section>

            <section className={style.section5}>
                <div className={style.container}>
                    <div className={style.headingWrapper}>
                        <h3>Unleash your potential</h3>
                    </div>
                    <div className={style.menuListRow}>
                        {dataSingleRow.map((ele)=><SingleRow dataSingleRow={ele}/>)}
                    </div>
                    <div className={style.buttonRow}>
                        <a href="" className={style.button}>
                            <span>
                                Explore our openings
                            </span>
                        </a>
                    </div>
                </div>
            </section>

        </div>
    )
}

export default AboutUs
