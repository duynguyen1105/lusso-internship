import React, { useEffect, useState } from 'react'
import logo from '../../assets/logo.svg'
import style from './style.module.scss'
import Submenu from './SubMenu/Submenu'
import {itemsServices, itemsHire} from './dataSubmenu'
import { Link } from 'react-router-dom'

const Navbar = () => {
    //TODO: Responsive 
    //TODO: Research how to clear / after the last item of navbar
    //TODO: Animation when hover to Contacct Us button ???
    
    const [scroll, setScroll] = useState(false);
    const [isHoverServices, setIsHoverServices] = useState(false)
    const [isHoverHire, setIsHoverHire] = useState(false)
    
    //handle scroll => change navbar
    useEffect(() => {
    window.addEventListener("scroll", () => {
        setScroll(window.scrollY > 80);
    });
    }, []);



    return (
        <div className={`${style.navBar} ${scroll&&style.sticky}`}>
            <div className={style.headerContainer}>
                <a href="/">
                    <img className={style.logo} src={logo} alt=""/>
                </a>
                <ul className={style.listPages} >
                    <Link to="/about-us">
                        <li className={style.listPagesItem} >
                            <a className={style.listPagesName} href="">About Us</a>
                        </li>
                    </Link>

                    <Link to="/">
                        <li className={style.listPagesItem}
                            onMouseEnter={() => setIsHoverServices(true)}
                            onMouseLeave={() => setIsHoverServices(false)}
                        >
                            <Submenu items={itemsServices} isHover={isHoverServices}/>
                            <a className={style.listPagesName} href="">Services</a>
                        </li>
                    </Link>


                    <li className={style.listPagesItem}
                        onMouseEnter={() => setIsHoverHire(true)}
                        onMouseLeave={() => setIsHoverHire(false)}
                    >
                        <Submenu items={itemsHire} isHover={isHoverHire}/>  
                        <a className={style.listPagesName} href="">Hire</a>
                    </li>
                    <li className={style.listPagesItem}>
                        <a className={style.listPagesName} href="">Case studies</a>
                    </li>
                    <li className={style.listPagesItem}>
                        <a className={style.listPagesName} href="">How it works</a>
                    </li>
                    <li className={style.listPagesItem}>
                        <a className={style.listPagesName} href="">Blog</a>
                    </li>
                </ul>
                <a href="" className={style.contactUs}> <span>Contact us</span> </a>
            </div>
        </div>
    )
}

export default Navbar
