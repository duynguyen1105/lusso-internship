import imageServices from '../../assets/subMenuNavbar/subServices'
import imageHire from '../../assets/subMenuNavbar/subHire'


export const itemsServices = [
    {
        img: imageServices.ai,
        title:['AI/ML Development Services'] ,
    },
    {
        img: imageServices.app,
        title: ['Application Development Services'],
    },
    {
        img: imageServices.web,
        title: [' Web Application Development Services'],
    },
    {  
        img: imageServices.api,
        title: ['API Integration' , 'Services'],
    },
    {  
        img: imageServices.customsw,
        title: ['Custom Software Development Services'],
    },           
    {  
        img: imageServices.swtest,
        title: ['Software Testing','Services'],
    },           
    {  
        img: imageServices.mobileapp,
        title: ['Mobile Application Development Services'],
    },           
    {  
        img: imageServices.swpro,
        title: ['Software Product Development Services'],
    },                      
    {  
        img: imageServices.ecom,
        title: ['Ecommerce' ,'Development Services'],
    },           
    {  
        img: imageServices.entapp,
        title: ['Enterprise  Application Development'],
    },           
]

export const itemsHire = [
    {
        img: imageHire.angular,
        title:['Hire Angular Developers'] ,
    },
    {
        img: imageHire.nodejs,
        title: ['Hire Nodejs Developers'],
    },
    {
        img: imageHire.reactjs,
        title: ['Hire Reactjs Developers'],
    },
    {  
        img: imageHire.dedicated,
        title: ['Hire Dedicated Developers'],
    },
    {  
        img: imageHire.rails,
        title: ['Hire Ruby on Rails Developers'],
    },           
    {  
        img: imageHire.reactnative,
        title: ['Hire React Native Developers'],
    },           
    {  
        img: imageHire.mobileApp,
        title: ['Hire Mobile App Developers'],
    },                        
    {  
        img: imageHire.dedicatedSw,
        title: ['Dedicated Software Development Team'],
    },           
    {  
        img: imageHire.python,
        title: ['Hire Python Developers'],
    },  
    {  
        img: imageHire.aspNet,
        title: ['Hire ASP.NET Developers'],
    },  
    {  
        img: imageHire.flutter,
        title: ['Hire Flutter Developers'],
    },           
    {  
        img: imageHire.php,
        title: ['Hire PHP Developers'],
    },
]

