import React from 'react'
import style from './style.module.scss'

const Submenu = ({items,isHover}) => {

    return (
        <div className={`${style.subMenu} ${isHover && style.hoverSubMenu}`}>
            <ul>
                
                {items.map((item) => (
                    <li>
                        <a href="">
                            <div className={style.icon}>
                                <img src={item.img} alt=""/>
                            </div>
                            <div className={style.title}>
                                {item.title.map((ele) => (
                                    <>
                                        {ele} <br/>
                                    </>
                                ))}
                            </div>
                        </a>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default Submenu
