import React from 'react'
import Navbar from './components/Navbar/Navbar'
import './App.scss'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import AboutUs from './components/AboutUs/AboutUs'

const App = () => {
  return (
    <div className="app">
      <Router>
        <Navbar/>
        <Switch>
          <Route exact path="/about-us">
            <AboutUs/>
          </Route>
        </Switch>
      </Router>
    </div>


  )
}

export default App
